import 'package:flutter/material.dart';

void main() => runApp(Myapp());

class Myapp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      //this is to change the theme
      theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Colors.purple,
          accentColor: Colors.blueAccent,
          textTheme: TextTheme(
              body1: TextStyle(
            fontSize: 24,
            fontStyle: FontStyle.italic,
          ))),

          //this is the actual app
      home: Scaffold(




        appBar: AppBar(//toolbar at the top
          title: Text('bulding'),
        ),


        body: Center(//body of the content
          child: Text(
            "this is the body",
            style: TextStyle(fontSize: 33),
          ),
        ),


        //floating button
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.lightbulb_outline),
          onPressed: () {
            print("you are right");
          },
        ),

        //footer buttons 
        persistentFooterButtons: <Widget>[
          IconButton(
            icon: Icon(Icons.add_comment),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.add_alarm),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.add_location),
            onPressed: () {},
          )
        ],



        
      ),
    );
  }
}
